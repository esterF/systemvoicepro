﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BLL;

namespace VoiceSystem.Controllers
{
    [RoutePrefix("api/b")]
    public class BettingController : ApiController
    {
        [Route("get")]
        [HttpGet]
        public IHttpActionResult get()
        {

            var t = GetImotApi.Connect();
            return Ok(t.Result);
        }
    }
}
